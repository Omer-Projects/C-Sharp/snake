﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    public class Snake
    {
        private int width, height;
        public int Width
        {
            get
            {
                return width;
            }
        }
        public int Height
        {
            get
            {
                return height;
            }
        }
        private Block[,] bord;
        public Block[,] Bord
        {
            get
            {
                return bord;
            }
        }
        private Random random;
        private List<Pixel> TheSnake;
        private bool Isload;
        private int point;
        public int Point
        {
            get
            {
                return point;
            }
            private set
            {
                point = value;
            }
        }
        private Step To = Step.Stop;
        private int xHead, yHead;

        public Snake(int Width, int Height)
        {
            width = Width;
            height = Height;
            bord = new Block[width, height];
            random = new Random();
            Isload = false;
            Point = 0;
            SetPixel = SetPixelDef;
            GameOver = GameOverDef;
            Collision = CollisionDef;
        }
        public Snake(Block[,] Bord)
        {
            bord = Bord;
            width = Bord.GetLength(0);
            height = Bord.GetLength(1);
            random = new Random();
            Isload = false;
            Point = 0;
            SetPixel = SetPixelDef;
            GameOver = GameOverDef;
            Collision = CollisionDef;
        }

        public void Load(int xStart = 1, int yStart = 1)
        {
            Isload = true;
            TheSnake = new List<Pixel>();
            Point = 0;
            Pixel haed = new Pixel(xStart, yStart, Block.Head);
            TheSnake.Add(haed);
            SetPixelPri(haed);
            SetObject();
            To = Step.Stop;
        }
        public bool Next()
        {
            if (!Isload || To == Step.Stop)
            {
                return false;
            }
            int xAdd = 0, yAdd = 0;
            switch (To)
            {
                case Step.Right:
                    xAdd = 1;
                    break;
                case Step.Up:
                    yAdd = -1;
                    break;
                case Step.Left:
                    xAdd = -1;
                    break;
                case Step.Down:
                    yAdd = 1;
                    break;
            }
            xHead += xAdd;
            yHead += yAdd;
            if (xHead < 0 || yHead < 0 || Width == xHead || Height == yHead)
            {
                GameOver(GameOverReason.Limt);
                return true;
            }
            Block typeTo = Bord[xHead, yHead];
            if (typeTo == Block.Snake || typeTo == Block.Head)
            {
                GameOver(GameOverReason.Snake);
                return true;
            }
            Pixel HeadTo = new Pixel(xHead, yHead, Block.Head);
            SetPixelPri(HeadTo);
            TheSnake.Add(HeadTo);
            Pixel use;
            if (typeTo != Block.Point)
            {
                use = TheSnake[0];
                SetPixelPri(new Pixel(use.X, use.Y, Block.Empty));
                TheSnake.RemoveAt(0);
            }
            else
            {
                SetObject();
                point++;
            }
            if (TheSnake.Count > 1)
            {
                use = TheSnake[TheSnake.Count - 2];
                SetPixelPri(new Pixel(use.X, use.Y, Block.Snake));
            }
            return true;
        }
        public void Move(Step step)
        {
            if (step != Step.Stop && To != Step.Stop)
            {
                if ((int)step % 2 == (int)To % 2)
                    return;
            }
            To = step;
        }
        public void UpdataSnake()
        {
            foreach (var item in TheSnake)
            {
                SetPixelPri(item);
            }
        }

        public void SetObject(bool IsPoint = true)
        {
            Block block = IsPoint ? Block.Point : Block.Object;
            int x, y;
            do
            {
                x = random.Next(0, Width);
                y = random.Next(0, Height);
            }
            while (Bord[x, y] != Block.Empty);
            SetPixelPri(new Pixel(x, y, block));
        }
        public Pixel[] GetTheSnake()
        {
            return TheSnake.ToArray();
        }

        private void SetPixelPri(Pixel pixel)
        {
            if (pixel.Type == Block.Head)
            {
                xHead = pixel.X;
                yHead = pixel.Y;
            }
            Bord[pixel.X, pixel.Y] = pixel.Type;
            SetPixel(pixel.X, pixel.Y, pixel.Type);
        }

        private void SetPixelDef(int x, int y, Block block) {}
        private void GameOverDef(GameOverReason reason) { }
        private void CollisionDef(Block item) { }

        public setPixel SetPixel;
        public gameOver GameOver;
        public collision Collision;
    }
    public enum Block
    {
        Empty,
        Snake,
        Head,
        Point,
        Object
    }
    public enum Step
    {
        Right,
        Up,
        Left,
        Down,
        Stop
    }
    public enum GameOverReason
    {
        Limt,
        Snake
    }
    public class Pixel
    {
        public int X, Y;
        public Block Type;

        public Pixel(int x, int y, Block type)
        {
            X = x;
            Y = y;
            Type = type;
        }
        public Pixel(int x, int y)
        {
            X = x;
            Y = y;
            Type = Block.Empty;
        }
        public Pixel()
        {
            X = 0;
            Y = 0;
            Type = Block.Empty;
        }
    }

    public delegate void setPixel(int x, int y, Block block);
    public delegate void gameOver(GameOverReason reason);
    public delegate void collision(Block item);
}
