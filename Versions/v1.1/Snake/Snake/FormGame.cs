﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class FormGame : Form
    {
        private Size SizeNow;
        private Image Screen;
        private Graphics g;

        private Snake TheGame;

        public FormGame()
        {
            InitializeComponent();
        }

        private void FormGame_Load(object sender, EventArgs e)
        {
            SizeNow = Size + Size.Empty;
            Size += new Size(16, 39);
            if (SizeNow.Width % BlokSize != 0 || SizeNow.Height % BlokSize != 0)
            {
                MessageBox.Show("Error in Limit");
                return;
            }
            Screen = new Bitmap(SizeNow.Width, SizeNow.Height);
            BordView.Location = Point.Empty;
            BordView.Size = SizeNow;
            g = Graphics.FromImage(Screen);
            g.Clear(Color.White);
            BordView.Image = Screen;

            TheGame = new Snake(SizeNow.Width / BlokSize, SizeNow.Height / BlokSize);
            TheGame.SetPixel = SetPixel;
            TheGame.GameOver = GameOver;

            TheGame.Load();

            timerUpdata.Start();
        }

        private void FormGame_KeyDown(object sender, KeyEventArgs e)
        {
            Step step;
            switch (e.KeyCode)
            {
                case Keys.P:
                case Keys.Space:
                    {
                        step = Step.Stop;
                    }
                    break;
                case Keys.A:
                case Keys.Left:
                    {
                        step = Step.Left;
                    }
                    break;
                case Keys.W:
                case Keys.Up:
                    {
                        step = Step.Up;
                    }
                    break;
                case Keys.D:
                case Keys.Right:
                    {
                        step = Step.Right;
                    }
                    break;
                case Keys.S:
                case Keys.Down:
                    {
                        step = Step.Down;
                    }
                    break;
                default:
                    {
                        return;
                    }
            }
            TheGame.Move(step);
        }

        private void timerUpdata_Tick(object sender, EventArgs e)
        {
            TheGame.Next();
            BordView.Image = Screen;
            Text = "Point: " + TheGame.Point;
        }

        private int BlokSize = 40;
        public void SetPixel(int x, int y, Block type)
        {
            x *= BlokSize;
            y *= BlokSize;
            Color color = Color.White;
            switch (type)
            {
                case Block.Empty:
                    color = Color.White;
                    break;
                case Block.Snake:
                    color = Color.Green;
                    break;
                case Block.Head:
                    color = Color.Blue;
                    break;
                case Block.Point:
                    color = Color.Red;
                    break;
                case Block.Object:
                    color = Color.Brown;
                    break;
            }
            g.FillRectangle(new SolidBrush(color), new Rectangle(x, y, BlokSize, BlokSize));
        }

        public void GameOver(GameOverReason reason)
        {
            timerUpdata.Stop();
            MessageBox.Show("Game Over\nPoints: " + TheGame.Point);
        }
    }
}
