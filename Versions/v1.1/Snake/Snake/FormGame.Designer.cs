﻿namespace Snake
{
    partial class FormGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerUpdata = new System.Windows.Forms.Timer(this.components);
            this.BordView = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.BordView)).BeginInit();
            this.SuspendLayout();
            // 
            // timerUpdata
            // 
            this.timerUpdata.Tick += new System.EventHandler(this.timerUpdata_Tick);
            // 
            // BordView
            // 
            this.BordView.Location = new System.Drawing.Point(38, 41);
            this.BordView.Name = "BordView";
            this.BordView.Size = new System.Drawing.Size(171, 189);
            this.BordView.TabIndex = 0;
            this.BordView.TabStop = false;
            // 
            // FormGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.BordView);
            this.Name = "FormGame";
            this.Text = "Snake";
            this.Load += new System.EventHandler(this.FormGame_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormGame_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.BordView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerUpdata;
        private System.Windows.Forms.PictureBox BordView;
    }
}

