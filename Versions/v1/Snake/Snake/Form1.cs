﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class FormGame : Form
    {
        private Color ColorClear, ColorSnake, ColorPoint;
        private Point Limit;

        public FormGame()
        {
            InitializeComponent();
        }

        private Random rand = new Random();
        private Bitmap bord;
        private int[,] pixels;
        private Graphics g;
        private int xStart, yStart;
        private int xEnd, yEnd;
        private int xPoint, yPoint;

        private int to = -1, points;
        class Step
        {
            public int To;
            public int Count;

            public Step(int to, int count)
            {
                To = to;
                Count = count;
            }
        }
        private List<Step> steps = new List<Step>();

        public int Points
        {
            get
            {
                return points;
            }
            set
            {
                points = value;
                Text = "Points: " + value;
            }
        }
        private Size SizeNow;

        private void FormGame_Load(object sender, EventArgs e)
        {
            ColorClear = Color.White;
            ColorSnake = Color.Green;
            ColorPoint = Color.Red;

            SizeNow = Size + Size.Empty;
            Size += new Size(16, 39);

            xStart = 1;
            yStart = 1;
            xEnd = xStart;
            yEnd = yStart;

            if (SizeNow.Width % BlokSize != 0 || SizeNow.Height % BlokSize != 0)
            {
                MessageBox.Show("Error in Limit");
                return;
            }
            Limit = new Point(SizeNow.Width / BlokSize, SizeNow.Height / BlokSize);
            pixels = new int[Limit.X + 1, Limit.Y + 1];
            bord = new Bitmap(SizeNow.Width, SizeNow.Height);
            g = Graphics.FromImage(bord);
            g.Clear(ColorClear);
            BordView.Location = Point.Empty;
            BordView.Size = SizeNow;

            SetPixel(xStart, yStart, ColorSnake, 1);
            SetPoint();

            Points = 0;

            MaximumSize = Size;
            MinimumSize = Size;

            timerUpdata.Start();
        }
        
        private void FormGame_KeyDown(object sender, KeyEventArgs e)
        {
            Keys key = e.KeyCode;
            if (key == Keys.P && false)//0
            {
                to = 0;
            }
            else
            {
                int toNow = 0;
                switch (key)
                {
                    case Keys.A:
                    case Keys.Left:
                        {
                            toNow = 1;
                        }
                        break;
                    case Keys.W:
                    case Keys.Up:
                        {
                            toNow = 2;
                        }
                        break;
                    case Keys.D:
                    case Keys.Right:
                        {
                            toNow = 3;
                        }
                        break;
                    case Keys.S:
                    case Keys.Down:
                        {
                            toNow = 4;
                        }
                        break;
                    default:
                        {
                            return;
                        }
                }

                if (toNow % 2 == to % 2 && to > 0)
                    return;
                if (to != 0)
                {
                    steps.Add(new Step(toNow, 0));
                }
                to = toNow;
            }
        }

        private void timerUpdata_Tick(object sender, EventArgs e)
        {
            BordView.Image = bord;
            if (to <= 0)
                return;
            int forward = ((to - 1) / 2 == 1) ? 1 : -1;
            if (to % 2 == 0)
            {
                yStart += forward;
                if (yStart < 0 || yStart == Limit.Y)
                {
                    GameOver();
                    return;
                }
            }
            else
            {
                xStart += forward;
                if (xStart < 0 || xStart == Limit.X)
                {
                    GameOver();
                    return;
                }
            }
            int type = GetPixel(xStart, yStart);
            if (type == 1)
            {
                GameOver();
                return;
            }
            SetPixel(xStart, yStart, ColorSnake, 1);
            if (steps.Count != 0)
                steps[steps.Count - 1].Count++;
            if (type == 2)
            {
                Points++;
                SetPoint();
            }
            else
            {
                SetPixel(xEnd, yEnd, ColorClear, 0);
                var Last = steps[0];
                if (Last.Count == 0)
                {
                    steps.RemoveAt(0);
                }
                Last = steps[0];
                Last.Count--;
                int forwardEnd = ((Last.To - 1) / 2 == 1) ? 1 : -1;
                if (Last.To % 2 == 0)
                {
                    yEnd += forwardEnd;
                }
                else
                {
                    xEnd += forwardEnd;
                }
            }
        }

        private int BlokSize = 40;
        public void SetPixel(int x, int y, Color color, int type)
        {
            pixels[x, y] = type;
            x *= BlokSize;
            y *= BlokSize;
            g.FillRectangle(new SolidBrush(color), new Rectangle(x, y, BlokSize, BlokSize));
        }
        private int GetPixel(int x, int y)
        {
            return pixels[x, y];
        }

        private void GameOver()
        {
            timerUpdata.Stop();
            MessageBox.Show("Game Over\nPoints: " + Points);
        }
        private void SetPoint()
        {
            int x, y;
            bool NotGood;
            do
            {
                x = rand.Next(0, Limit.X);
                y = rand.Next(0, Limit.Y);
                NotGood = GetPixel(x, y) != 0;
                if (Points < 10)
                {
                    bool xL = x == 0 || x == Limit.X;
                    bool yL = y == 0 || y == Limit.Y;
                    if (Points < 5)
                    {
                        NotGood |= xL || yL;
                    }
                    else
                    {
                        NotGood |= xL && yL;
                    }

                }
            }
            while (NotGood);

            SetPixel(x, y, ColorPoint, 2);
            xPoint = x;
            yPoint = y;
        }
    }
}
